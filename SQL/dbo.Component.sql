﻿CREATE TABLE [dbo].[Component] (
    [IDComponent] INT            IDENTITY (1, 1) NOT NULL,
    [Name]        NVARCHAR (MAX) NULL,
    [Description] NVARCHAR (MAX) NULL,
    [IDUser]      INT            NULL,
    PRIMARY KEY CLUSTERED ([IDComponent] ASC),
    CONSTRAINT [FK_Component_User] FOREIGN KEY ([IDUser]) REFERENCES [dbo].[User] ([IDUser]) ON DELETE CASCADE
);

