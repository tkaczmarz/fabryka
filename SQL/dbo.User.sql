﻿CREATE TABLE [dbo].[User] (
    [IDUser]  INT            IDENTITY (1, 1) NOT NULL,
    [Name]    NVARCHAR (MAX) NULL,
    [Surname] NVARCHAR (MAX) NULL,
    [Email]   NVARCHAR (MAX) NOT NULL,
    PRIMARY KEY CLUSTERED ([IDUser] ASC)
);

