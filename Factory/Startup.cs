﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Factory.Startup))]
namespace Factory
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
