# Fabryka

Jest to prosty serwis internetowy korzystający z bazy danych. Baza posiada dwie tabele w relacji 1:n. Aplikacja umożliwia dodawanie pracowników oraz części przez nich wyprodukowanych.

Projekt został wykonany wykorzystując technologie ASP.NET w języku C#. Zastosowałem wzór projektowy MVC.